package com.citi.training.trade.rest;

import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import com.citi.training.trade.dai.TradeDataAccessInterface;
import com.citi.training.trade.model.Trade;
import com.fasterxml.jackson.databind.ObjectMapper;


@RunWith(SpringRunner.class)
@WebMvcTest(TradeController.class)
public class TradeControllerTests {


    private static final Logger logger = LoggerFactory.getLogger(TradeControllerTests.class);

    @Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private TradeDataAccessInterface tradeDai;

    @Test
    public void findAllStocks_returnsList() throws Exception {
        when(tradeDai.findAll()).thenReturn(new ArrayList<Trade>());

        MvcResult result = this.mockMvc.perform(get("/trade")).andExpect(status().isOk())
                .andExpect(jsonPath("$.size()").isNumber()).andReturn();

        logger.info("Result from tradeDai.findAll: " +
                    result.getResponse().getContentAsString());
    }


    @Test
    public void getStockById_returnsOK() throws Exception {
        Trade testTrade = new Trade(5, "AAPL", 1, 100000, 1000);

        when(tradeDai.findById(testTrade.getId())).thenReturn(
                                                    Optional.of(testTrade));

        MvcResult result = this.mockMvc.perform(get("/trade/" + testTrade.getId()))
                                       .andExpect(status().isOk()).andReturn();

        logger.info("Result from tradeDai.getTrade: " +
                    result.getResponse().getContentAsString());
    }
	
}
