package com.citi.training.trade.rest;

import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import com.citi.training.trade.model.Trade;


@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = WebEnvironment.RANDOM_PORT)
@ActiveProfiles("h2")
public class TradeControllerIntegrationTests  {

    private static final Logger LOG = LoggerFactory.getLogger(TradeControllerIntegrationTests .class);

    // @Autowired - allow to access the Beans inside the container from else where.
    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void getTrade_returnsTrade() {
        String testStock = "AAPL";
        int testBuy = 1;
        double testPrice = 1000000;
        int testVolume = 1000;
        

        // Do a HTTP Post to /trade
        ResponseEntity<Trade> createTradeResponse = restTemplate.postForEntity("/trade",
                                             new Trade(-1, testStock, testBuy, testPrice, testVolume),
                                             Trade.class);

        LOG.info("Create Stock response: " + createTradeResponse.getBody());
        assertEquals(HttpStatus.CREATED, createTradeResponse.getStatusCode());
        assertEquals("created Trade stock should equal test stock",
        		testStock, createTradeResponse.getBody().getStock());
        assertEquals("created Trade buy should equal test buy",
                testBuy, createTradeResponse.getBody().getBuy());
        assertEquals("created Trade price should equal test price",
                testPrice, createTradeResponse.getBody().getPrice(), 0.0001);
        assertEquals("created Trade volume should equal test volume",
                testVolume, createTradeResponse.getBody().getVolume());
        
        
        
        // Do A HTTP GET to /trade/ID     
        ResponseEntity<Trade> findTradeResponse = restTemplate.getForEntity(
        		"/trade/" + createTradeResponse.getBody().getId(), Trade.class);
        
        LOG.info("FindById response: " + findTradeResponse.getBody());
        assertEquals(HttpStatus.OK, findTradeResponse.getStatusCode());
        assertEquals("Founded Trade stock should equal test stock",
                testStock, findTradeResponse.getBody().getStock());
        assertEquals("Founded Trade buy should equal test buy",
                testBuy, findTradeResponse.getBody().getBuy());
        assertEquals("Founded Trade price should equal test price",
                testPrice, findTradeResponse.getBody().getPrice(), 0.0001);
        assertEquals("Founded Trade volume should equal test volume",
                testVolume, findTradeResponse.getBody().getVolume());
        
        
        // Do A HTTP DELETE to /trade/ID
        //restTemplate.delete("/trade/" + createTradeResponse.getBody().getId());
        ResponseEntity<Trade> DeleteTradeResponse = restTemplate.exchange(
        		"/trade/" + createTradeResponse.getBody().getId(),
        		HttpMethod.DELETE, null, new ParameterizedTypeReference<Trade>() {});

        
        // Do A HTTP GET to /trade/ID to test the DefaultExceptionHandler
        ResponseEntity<Trade> findTradeResponse2 = restTemplate.getForEntity(
        		"/trade/999", Trade.class);
        
        LOG.info("FindById response: " + findTradeResponse2.getBody());
        assertEquals(HttpStatus.NOT_FOUND, findTradeResponse2.getStatusCode());
    }
}
