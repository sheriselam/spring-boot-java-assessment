package com.citi.training.trade.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import org.junit.Test;

public class TradeTests {
	
	private int testId = 3;
	
	private String testStock = "APPL";
	// a stock symbol e.g. APPL, GOOGL, MSFT 
	
	private int testBuy = 1;
	// either 1 or 0 -- 1=trade is a buy; 0=trade is sell.
	
	private double testPrice = 100000;
	// a dollar amount at which this trade will be made 

	private int testVolume = 1000;
	// number of stocks to be traded.
	
	@Test
    public void test_Trade_defaultConstructor() {
        Trade testTrade = new Trade();

        testTrade.setId(testId);
        testTrade.setStock(testStock);
        testTrade.setBuy(testBuy);
        testTrade.setPrice(testPrice);
        testTrade.setVolume(testVolume);

        assertEquals("Trade id should be equal to testId",
                    	testId, testTrade.getId());
        assertEquals("Trade stock should be equal to testStock",
        				testStock, testTrade.getStock());
        assertEquals("Trade buy should be equal to testBuy",
        				testBuy, testTrade.getBuy());
        assertEquals("Trade price should be equal to testPrice",
        				testPrice, testTrade.getPrice(), 0.0001);
        assertEquals("Trade volume should be equal to testVolume",
        				testVolume, testTrade.getVolume());
    }

    @Test
    public void test_Trade_fullConstructor() {
    	Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

    	assertEquals("Trade id should be equal to testId",
            			testId, testTrade.getId());
		assertEquals("Trade stock should be equal to testStock",
						testStock, testTrade.getStock());
		assertEquals("Trade buy should be equal to testBuy",
						testBuy, testTrade.getBuy());
		assertEquals("Trade price should be equal to testPrice",
						testPrice, testTrade.getPrice(), 0.0001);
		assertEquals("Trade volume should be equal to testVolume",
						testVolume, testTrade.getVolume());
    }

    @Test
    public void test_Stock_toString() {
    	Trade testTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
       
    	assertTrue("toString should contain testId",
    				testTrade.toString().contains(Integer.toString(testId)));
        assertTrue("toString should contain testStock",
        			testTrade.toString().contains(testStock));
        assertTrue("toString should contain testBuy",
        			testTrade.toString().contains(Integer.toString(testBuy)));
        assertTrue("toString should contain testPrice",
        			testTrade.toString().contains(Double.toString(testPrice)));
        assertTrue("toString should contain testVolume",
        			testTrade.toString().contains(Integer.toString(testVolume)));
        
    }
    
    @Test
    public void test_Trade_equals() {
        Trade firstTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

        Trade compareTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);
        assertFalse("Trade for test should not be the same object",
        				firstTrade == compareTrade);
        assertTrue("Trade for test should be found to be equal",
        				firstTrade.equals(compareTrade));
    }

    @Test
    public void test_Trade_equalsFails() {
    	Trade firstTrade = new Trade(testId + 1, testStock, testBuy, testPrice, testVolume);
    	Trade secondTrade = new Trade(testId, testStock + "_test", testBuy, testPrice, testVolume);
        Trade thirdTrade = new Trade(testId, testStock, 0, testPrice, testVolume);
        Trade fourthTrade = new Trade(testId, testStock, testBuy, testPrice + 10000, testVolume);
        Trade fifthTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume + 500);
        Trade[] createdTrade = {firstTrade, secondTrade, thirdTrade, fourthTrade, fifthTrade};

        Trade compareTrade = new Trade(testId, testStock, testBuy, testPrice, testVolume);

        for(Trade thisTrade: createdTrade) {
            assertFalse("trade for test should not be the same object",
            				thisTrade == compareTrade);
            assertFalse("trade for test should not be found to be equal",
            				thisTrade.equals(compareTrade));
        }
    }
	
}
