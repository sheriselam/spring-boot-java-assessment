package com.citi.training.trade.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

/**
 * Declaring all column in a row inside the Trade table.
 * 
 * @author Sherise Sin Yee Lam
 */

@Entity
public class Trade {

	@Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
	private int id;
	
	private String stock;
	// a stock symbol e.g. AAPL, GOOGL, MSFT 
	
	private int buy;
	// either 1 or 0 -- 1=trade is a buy; 0=trade is sell.
	
	private double price;
	// a dollar amount at which this trade will be made 

	private int volume;
	// number of stocks to be traded.
	
	public Trade(){}
	
	public Trade(int id, String stock, int buy, double price, int volume) {
		this.id = id;
		this.stock = stock;
		this.buy = buy;
		this.price = price;
		this.volume = volume;
	}
	
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	
	public String getStock() {
		return stock;
	}
	public void setStock(String stock) {
		this.stock = stock;
	}
	
	public int getBuy() {
		return buy;
	}
	public void setBuy(int buy) {
		this.buy = buy;
	}
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	public int getVolume() {
		return volume;
	}
	public void setVolume(int volume) {
		this.volume = volume;
	}
	
	@Override
	public String toString() {
		return "Trade [id=" + id + ", stock=" + stock + ", buy=" + buy + ", price=" + price + ", volume=" + volume
				+ "]";
	}
	
	public boolean equals(Trade trade) {
        return this.id == trade.getId() &&
               this.stock.equals(trade.getStock()) &&
               this.buy == trade.getBuy() &&
               this.price == trade.getPrice() &&
               this.volume == trade.getVolume();
    }
	
}
