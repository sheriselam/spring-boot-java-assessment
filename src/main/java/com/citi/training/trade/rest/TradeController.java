package com.citi.training.trade.rest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.citi.training.trade.dai.TradeDataAccessInterface;
import com.citi.training.trade.model.Trade;

/**
 * This is the REST interface for managing Trade records.
 * 
 * @author Sherise Sin Yee Lam
 * 
 * @see Trade
 * for all the columns' details.
 */

@RestController
@RequestMapping("/trade")
public class TradeController {

	private static final Logger LOG = LoggerFactory.getLogger(TradeController.class);

    @Autowired
    private TradeDataAccessInterface tradeDai;

    @RequestMapping(method=RequestMethod.GET)
    public Iterable<Trade> findAll() {
        LOG.info("HTTP GET findAll()");
        return tradeDai.findAll();
    }
    
    @RequestMapping(value="/{id}", method=RequestMethod.GET)
    public Trade findById(@PathVariable int id) {
        LOG.info("HTTP GET findById() id=[" + id + "]");
        return tradeDai.findById(id).get();
    }

    @RequestMapping(method=RequestMethod.POST)
    public HttpEntity<Trade> save(@RequestBody Trade trade) {
        LOG.info("HTTP POST save() trade=[" + trade + "]");
        return new ResponseEntity <Trade>(tradeDai.save(trade),
        									HttpStatus.CREATED);
    }

    @RequestMapping(value="/{id}", method=RequestMethod.DELETE)
    @ResponseStatus(value=HttpStatus.NO_CONTENT)
    public void deleteById(@PathVariable int id) {
        LOG.info("HTTP DELETE delete() id=[" + id + "]");
        tradeDai.deleteById(id);
    }
	
}
