package com.citi.training.trade.dai;

import org.springframework.data.repository.CrudRepository;

import com.citi.training.trade.model.Trade;


/** 
 * A Data Assess Interface for the trade table.
*/
public interface TradeDataAccessInterface extends CrudRepository<Trade, Integer>{

}
